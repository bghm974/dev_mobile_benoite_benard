package com.example.taps;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Random;

public class playground extends AppCompatActivity {

    boolean game = true;
    boolean t1 = false;
    boolean t2 = false;
    boolean t3 = false;
    boolean t4 = false;
    int time = 1000;
    int score = 0;
    int pass_1 = 0;
    int pass_2 = 0;
    int pass_3 = 0;
    int pass_4 = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playground);


        final Intent intent_main = new Intent(this,MainActivity.class);
        final Intent intent_askScore = new Intent(this, AskScore.class);

        final TextView scoreView = findViewById(R.id.score);

        ImageButton quit_button = findViewById(R.id.quit_button);
        final ImageButton taupe_1 = findViewById(R.id.taupe_1);
        final ImageButton taupe_2 = findViewById(R.id.taupe_2);
        final ImageButton taupe_3 = findViewById(R.id.taupe_3);
        final ImageButton taupe_4 = findViewById(R.id.taupe_4);

        taupe_1.setVisibility(View.INVISIBLE);
        taupe_2.setVisibility(View.INVISIBLE);
        taupe_3.setVisibility(View.INVISIBLE);
        taupe_4.setVisibility(View.INVISIBLE);

        quit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent_main);
                finish();
            }
        });

        taupe_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pass_1=0;
                if (t1){
                    score++;
                    t1=false;
                    taupe_1.setVisibility(View.INVISIBLE);
                }
                else {
                    game=false;
                }
            }
        });
        taupe_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(game){
                    pass_2=0;
                    if (t2){
                        score++;
                        t2=false;
                        taupe_2.setVisibility(View.INVISIBLE);
                    }
                    else{
                        game=false;
                    }
                }
            }
        });
        taupe_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(game){
                    pass_3=0;
                    if (t3){
                        score++;
                        t3=false;
                        taupe_3.setVisibility(View.INVISIBLE);
                    }
                    else{
                        game=false;
                    }
                }
            }
        });
        taupe_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(game){
                    pass_4=0;
                    if (t4){
                        score++;
                        t4=false;
                        taupe_4.setVisibility(View.INVISIBLE);
                    }
                    else {
                        game=false;
                    }
                }
            }
        });

        class Game implements Runnable{
            @Override
            public void run() {
                Random r = new Random();
                while (game){
                    int taupe = r.nextInt((4-1)+1)+1;
                    switch (taupe){
                        case 1:
                            if (!t1){
                                taupe = r.nextInt((10-1)+1)+1;
                                if (taupe>2){
                                    t1=true;
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            taupe_1.setImageResource(R.drawable.tp);
                                            taupe_1.setVisibility(View.VISIBLE);
                                        }
                                    });
                                }
                                else {
                                    if (taupe_1.isShown()){
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_1.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                    }
                                    else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_1.setImageResource(R.drawable.tp_red);
                                                taupe_1.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                }
                            }
                            else {
                                if (pass_1==1){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            taupe_1.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                    game=false;
                                }
                                else{
                                    pass_1++;
                                }
                            }
                            break;
                        case 2:
                            if (!t2){
                                taupe = r.nextInt((10-1)+1)+1;
                                if (taupe>2){
                                    t2=true;
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            taupe_2.setImageResource(R.drawable.tp);
                                            taupe_2.setVisibility(View.VISIBLE);
                                        }
                                    });
                                }
                                else {
                                    if (taupe_2.isShown()){
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_2.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                    }
                                    else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_2.setImageResource(R.drawable.tp_red);
                                                taupe_2.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                }
                            }
                            else {
                                if (pass_2==1){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            taupe_2.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                    game=false;
                                }
                                else{
                                    pass_2++;
                                }
                            }
                            break;
                        case 3:
                            if (!t3){
                                taupe = r.nextInt((10-1)+1)+1;
                                if (taupe>2){
                                    t3=true;
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            taupe_3.setImageResource(R.drawable.tp);
                                            taupe_3.setVisibility(View.VISIBLE);
                                        }
                                    });
                                }
                                else {
                                    if (taupe_3.isShown()){
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_3.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                    }
                                    else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_3.setImageResource(R.drawable.tp_red);
                                                taupe_3.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                }
                            }
                            else {
                                if (pass_3==1){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            taupe_3.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                    game=false;
                                }
                                else{
                                    pass_3++;
                                }
                            }
                            break;
                        case 4:
                            if (!t4){
                                taupe = r.nextInt((10-1)+1)+1;
                                if (taupe>2){
                                    t4=true;
                                    runOnUiThread(new Runnable() {
                                        public void run() {
                                            taupe_4.setImageResource(R.drawable.tp);
                                            taupe_4.setVisibility(View.VISIBLE);
                                        }
                                    });
                                }
                                else {
                                    if (taupe_4.isShown()){
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_4.setVisibility(View.INVISIBLE);
                                            }
                                        });
                                    }
                                    else {
                                        runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                taupe_4.setImageResource(R.drawable.tp_red);
                                                taupe_4.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                }
                            }
                            else {
                                if (pass_4==1){
                                    runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            taupe_4.setVisibility(View.INVISIBLE);
                                        }
                                    });
                                    game=false;
                                }
                                else{
                                    pass_4++;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                    if (time>=200){
                        time-=25;
                    }
                    if(pass_1 == 0 && pass_2==0 && pass_3==0 && pass_4==0){
                        try {
                            Thread.sleep(time);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    else{
                        try {
                            Thread.sleep(2*time);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        class UpdateScore implements Runnable{
            @Override
            public void run() {
                while (game){
                    runOnUiThread(new Runnable() {
                        public void run() {
                            scoreView.setText(String.valueOf(score));
                        }
                    });
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

        class StopGame implements Runnable{
            @Override
            public void run() {
                while (true){
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    if (!game){
                        intent_askScore.putExtra("score",score);
                        startActivity(intent_askScore);
                        finish();
                        break;
                    }
                }
            }
        }

        Game taps = new Game();
        new Thread(taps).start();
        UpdateScore refreshScore = new UpdateScore();
        new Thread(refreshScore).start();
        StopGame stop = new StopGame();
        new Thread(stop).start();
    }
}
