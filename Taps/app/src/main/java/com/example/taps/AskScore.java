package com.example.taps;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class AskScore extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ask_score);

        final Intent intent_main = new Intent(this,MainActivity.class);

        final Intent intent_askScore = getIntent();
        final int score = intent_askScore.getIntExtra("score",0);

        TextView scoreView = findViewById(R.id.scoreView);
        final EditText nameField = findViewById(R.id.textFieldScore);
        Button yes_button = findViewById(R.id.yes_button);
        Button no_button = findViewById(R.id.no_button);

        scoreView.setText(String.valueOf(score));

        class MaBase extends SQLiteOpenHelper {
            private static final String TABLE_SCORE = "table_score";
            private static final String COL_ID = "id";
            private static final String COL_NAME = "col_name";
            private static final String COL_SCORE = "col_score";

            private static final String CREATE_BDD = "CREATE TABLE "+TABLE_SCORE+" ("+COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_NAME+" TEXT NOT NULL, "+COL_SCORE+" INTEGER);";

            public MaBase(Context context, String name, SQLiteDatabase.CursorFactory factory,int version){
                super(context,name,factory,version);
            }

            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(CREATE_BDD);
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {db.execSQL("DROP TABLE " + TABLE_SCORE + ";");
                onCreate(db);
            }
        }

        class ScoreBDD {
            private static final int VERSION_BDD=1;
            private static final String NOM_BDD = "score.db";

            private static final String TABLE_SCORE = "table_score";
            private static final String COL_ID = "id";
            private static final int NUM_COL_ID = 0;
            private static final String COL_NAME = "col_name";
            private static final int NUM_COL_NAME = 1;
            private static final String COL_SCORE = "col_score";
            private static final int NUM_COL_SCORE = 2;

            private SQLiteDatabase bdd;

            private MaBase maBase;

            public ScoreBDD(Context context){
                //On crée la BDD et sa table
                maBase = new MaBase(context, NOM_BDD, null, VERSION_BDD);
            }

            public void open(){
                //on ouvre la BDD en écriture
                bdd = maBase.getWritableDatabase();
            }

            public void close(){
                //on ferme l'accès à la BDD
                bdd.close();
            }


            public SQLiteDatabase getBDD(){
                return bdd;
            }

            public long insertScore(String perso, int score){
                //Création d'un ContentValues (fonctionne comme une HashMap)
                ContentValues values = new ContentValues();
                //on lui ajoute une valeur associée à une clé (qui est le nom de la colonne dans laquelle on veut mettre la valeur)
                values.put(COL_NAME, nameField.getText().toString());
                values.put(COL_SCORE, score);
                //on insère l'objet dans la BDD via le ContentValues
                return bdd.insert(TABLE_SCORE, null, values);
            }

            public int updateScore(int id, String perso,int score) {
                //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
                //il faut simplement préciser quel livre on doit mettre à jour grâce à l'ID
                ContentValues values = new ContentValues();
                values.put(COL_NAME, perso);
                values.put(COL_SCORE, score);
                return bdd.update(TABLE_SCORE, values, COL_ID + " = " + id, null);
            }

            public int removeScoreWithID(int id){
                //Suppression d'un livre de la BDD grâce à l'ID
                return bdd.delete(TABLE_SCORE, COL_ID + " = " +id, null);
            }
        }

        no_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(intent_main);
                finish();
            }
        });

        yes_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               ScoreBDD scoreBD = new ScoreBDD(v.getContext());
               scoreBD.open();
               scoreBD.insertScore(nameField.getText().toString(),score);
               scoreBD.close();
                startActivity(intent_main);
                finish();
            }
        });

    }
}
