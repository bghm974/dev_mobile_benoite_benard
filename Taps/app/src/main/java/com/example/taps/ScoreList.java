package com.example.taps;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;

public class ScoreList extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_list);

        class MaBase extends SQLiteOpenHelper {
            private static final String TABLE_SCORE = "table_score";
            private static final String COL_ID = "id";
            private static final String COL_NAME = "col_name";
            private static final String COL_SCORE = "col_score";

            private static final String CREATE_BDD = "CREATE TABLE "+TABLE_SCORE+" ("+COL_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, "+COL_NAME+" TEXT NOT NULL, "+COL_SCORE+" INTEGER);";

            public MaBase(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
                super(context,name,factory,version);
            }

            @Override
            public void onCreate(SQLiteDatabase db) {
                db.execSQL(CREATE_BDD);
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {db.execSQL("DROP TABLE " + TABLE_SCORE + ";");
                onCreate(db);
            }
        }

        class ScoreBDD {
            private static final int VERSION_BDD=1;
            private static final String NOM_BDD = "score.db";

            private static final String TABLE_SCORE = "table_score";
            private static final String COL_ID = "id";
            private static final int NUM_COL_ID = 0;
            private static final String COL_NAME = "col_name";
            private static final int NUM_COL_NAME = 1;
            private static final String COL_SCORE = "col_score";
            private static final int NUM_COL_SCORE = 2;

            private SQLiteDatabase bdd;

            private MaBase maBase;

            public ScoreBDD(Context context){
                //On crée la BDD et sa table
                maBase = new MaBase(context, NOM_BDD, null, VERSION_BDD);
            }

            public void open(){
                //on ouvre la BDD en écriture
                bdd = maBase.getWritableDatabase();
            }

            public void close(){
                //on ferme l'accès à la BDD
                bdd.close();
            }


            public SQLiteDatabase getBDD(){
                return bdd;
            }

            public int updateScore(int id, String perso,int score) {
                //La mise à jour d'un livre dans la BDD fonctionne plus ou moins comme une insertion
                //il faut simplement préciser quel livre on doit mettre à jour grâce à l'ID
                ContentValues values = new ContentValues();
                values.put(COL_NAME, perso);
                values.put(COL_SCORE, score);
                return bdd.update(TABLE_SCORE, values, COL_ID + " = " + id, null);
            }

            public int removeScoreWithID(int id){
                //Suppression d'un livre de la BDD grâce à l'ID
                return bdd.delete(TABLE_SCORE, COL_ID + " = " +id, null);
            }

            public Cursor getRows(){
                String where = null;
                Cursor c = bdd.rawQuery("SELECT * FROM table_score ORDER BY col_score DESC",null);
                return c;
            }
        }

        ScoreBDD scoreBD = new ScoreBDD(getApplicationContext());
        scoreBD.open();
        ListView list = findViewById(R.id.ScoreScroll);
        ArrayList<String> perso = new ArrayList<>();
        ArrayList<String> scoreperso = new ArrayList<>();

        Cursor data = scoreBD.getRows();
        if (data==null){
            Toast.makeText(ScoreList.this,"Pas de score enregistré !",Toast.LENGTH_LONG ).show();
        }
        else {
            while (data.moveToNext()){
                perso.add(data.getString(1));
                scoreperso.add(data.getString(2));

                ArrayList<HashMap<String,String>> map = new ArrayList<HashMap<String,String>>();
                HashMap<String,String> item;

                for(int i=0;i<perso.size();i++){
                    item = new HashMap<String,String>();
                    item.put("line1",perso.get(i));
                    item.put( "line2", scoreperso.get(i));
                    map.add( item );
                }

                SimpleAdapter persoAdapter;
                persoAdapter = new SimpleAdapter(this,map,android.R.layout.two_line_list_item,new String[] { "line1","line2" },new int[]{android.R.id.text1,android.R.id.text2});
                ((ListView)findViewById(R.id.ScoreScroll)).setAdapter(persoAdapter);
            }
        }
    }
}
